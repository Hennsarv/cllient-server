﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp27
{
    public partial class Server : Form
    {
        public void Write(string s)
        {
            this.richTextBox1.Text += s + "\n";
        }

        public Server()
        {
            InitializeComponent();
        }
    }
}
